package controller;

import java.text.ParseException;
import java.util.Date;

import API.SistemaRecomendacionPeliculas;
import data_structures.ARN;
import data_structures.EncadenamientoSeparadoGenero;
import data_structures.ListaEncadenada;
import modelo.Cargador;
import vos.VOGeneroPelicula;
import vos.VOPelicula;
import vos.VOTag;
import vos.VOTagSegmento;
import vos.VOUsuario;

public class Controller {

	private static Cargador a = new Cargador();
	private static SistemaRecomendacionPeliculas b = new SistemaRecomendacionPeliculas();
	
	public static ListaEncadenada<VOUsuario> darUsuarios()
	{
		return a.darUsuarios();
	}
	public static boolean cargarRatings(String pRuta) {
		return a.cargarRatingsSR(pRuta);
	}
	public static boolean cargarTags(String pRuta) 
	{
		return a.cargarTagsSR(pRuta);
	}
	public static boolean cargarCategorias(String pRuta) {
		return a.cargarCategoriasTags(pRuta);
	}

	public static ListaEncadenada<VOTag> darTags() {
		return a.darTags();
	}
	public static void cargarUsuarios()
	{
		a.cargarUsuarios();
	}
	public static ListaEncadenada<VOUsuario> darUsuariosPorSegmento(String pSegmento)
	{
		return a.darUsuariosPorSegmento(pSegmento);
	}
	public static void segmentarUsuarios()
	{
		a.SegmentarUsuarios();
	}
	public static ListaEncadenada<VOTagSegmento> darTagsSegmento()
	{
		return a.darTagsSegmento();
	}
	
	public static void cargarInfoJson() throws ParseException
	{
		a.cargarInfoJson();
	}
	public static VOPelicula[] darPelis() {
		return a.darPeliculas();
	}
	public static void req1(int idUsuario, String ruta)
	{
		b.registrarSolicitudRecomendacion(idUsuario, ruta);
	}
	public static EncadenamientoSeparadoGenero<String, ARN<Date, VOPelicula>> darEncadenamiento() {
		return a.darEncadenamiento();
	}
	
	public static ListaEncadenada<VOPelicula> darPeliculasRango(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		return a.peliculasGenero(genero, fechaInicial, fechaFinal);
	}
}
