package vos;

public class VOUsuario implements Comparable<VOUsuario> {

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;
	private String conformismo;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	public String getConformismo() {
		return conformismo;
	}
	public void setConformismo(String conformismo) {
		this.conformismo = conformismo;
	}
	@Override
	public int compareTo(VOUsuario o) {
		int respuesta = 0; 
		if(this.getIdUsuario() == o.getIdUsuario()) { respuesta = 0;}
		if (this.getIdUsuario() < o.getIdUsuario()) { respuesta = -1;}
		if (this.getIdUsuario() > o.getIdUsuario()) { respuesta = 1;} 
		
		if (this.getPrimerTimestamp() == o.getPrimerTimestamp()) { respuesta = 0;} 
		if (this.getPrimerTimestamp() < o.getPrimerTimestamp()) { respuesta = -1;} 
		if (this.getPrimerTimestamp() > o.getPrimerTimestamp()) {respuesta = 1;} 
		
		if (this.getNumRatings() == o.getNumRatings()) { respuesta = 0;}
		if (this.getNumRatings() < o.getNumRatings()) { respuesta = -1;} 
		if (this.getNumRatings() > o.getNumRatings()) { respuesta = 1;} 
		
		if (this.getDiferenciaOpinion() == o.getDiferenciaOpinion()) { respuesta = 0;}
		if (this.getDiferenciaOpinion() < o.getDiferenciaOpinion()) { respuesta = -1;} 
		if (this.getDiferenciaOpinion() > o.getDiferenciaOpinion()) { respuesta = 1;} 
		
		
		return respuesta; 
		
		
	}
	public boolean equals(VOUsuario o){
		if (this.getIdUsuario() == o.getIdUsuario() &&
				this.getPrimerTimestamp() == o.getPrimerTimestamp() &&
				this.getNumRatings() == o.getNumRatings() &&
				this.getDiferenciaOpinion() == o.getDiferenciaOpinion()){ return true;}
		return false; 
		
	}
	
	
}
