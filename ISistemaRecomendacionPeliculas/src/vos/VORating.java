package vos;

public class VORating implements Comparable<VORating>{
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	private double error; 
	public double getError() {
		return error;
	}

	public void setError(double error) {
		this.error = error;
	}

	public long getIdUsuario() {
		return idUsuario;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	@Override
	public int compareTo(VORating o) {
		int respuesta = 0; 
		if(this.getIdUsuario() == o.getIdUsuario()){ respuesta = 0;} 
		if(this.getIdUsuario() < o.getIdUsuario()){ respuesta = -1;} 
		if (this.getIdUsuario() > o.getIdUsuario()) {respuesta = 1;} 
		
		if (this.getIdPelicula() == o.getIdPelicula()) { respuesta = 0;} 
		if (this.getIdPelicula() < o.getIdPelicula()) {respuesta = -1;} 
		if(this.getIdPelicula() > o.getIdPelicula()) { respuesta = 1;} 
		
		if (this.getRating() == o.getRating()) { respuesta = 0;}
		if (this.getRating() < o.getRating()) { respuesta = -1;} 
		if (this.getRating() > o.getRating()) { respuesta = 1;} 
		return respuesta; 
	}
	public boolean equals(VORating o){
		if (this.getIdUsuario() == o.getIdUsuario() &&
				this.getIdPelicula() == o.getIdPelicula() && 
				this.getRating() == o.getRating()){
			return true; 
		}
		else
			return false; 
	}
	

}
