package vos;

import data_structures.ILista;
import data_structures.ListaEncadenada;

public class VOSegmentoUsuario implements Comparable<VOSegmentoUsuario>{

	private String genero;

	private ListaEncadenada<VOPelicula> peliculas;

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public ListaEncadenada<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ListaEncadenada<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}

	@Override
	public int compareTo(VOSegmentoUsuario o) {
		int respuesta = 0; 
		if (this.getGenero().compareTo(o.getGenero()) == 0){ respuesta = 0;}
		if (this.getGenero().compareTo(o.getGenero()) < 0){ respuesta = -1;}
		if (this.getGenero().compareTo(o.getGenero()) > 0){ respuesta = 1;}
		
		return respuesta; 
	}
	public boolean equals(VOSegmentoUsuario o){
		if(this.getGenero().compareTo(o.getGenero()) == 0){return true;}
		return false; 
	}
	





} 


