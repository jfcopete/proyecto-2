package API;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import data_structures.ARN;
import data_structures.ListaEncadenada;
import data_structures.ListaSimpleEncadenada;
import data_structures.MaxPQ;
import data_structures.SeparateChainingHashST;
import modelo.Cargador;
import vos.VOGeneroPelicula;
import vos.VOInfoPelicula;
import vos.VOPelicula;
import vos.VOReporteSegmento;
import vos.VOUsuarioPelicula;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private MaxPQ<ClaseRegistrarSolicitud> solicitudes = new MaxPQ<>(); 
	ListaEncadenada<ClaseRegistrarSolicitud> listaSolicitudes = new ListaEncadenada<>();
	ARN<Integer,SeparateChainingHashST<String, ARN<Double,ListaSimpleEncadenada<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>> >>> arbolOrdenAnho;
	private Cargador cargar = new Cargador();
	VOInfoPelicula peliculas[] = null; 
	@Override
	public ISistemaRecomendacionPeliculas crearSR() {
		return new SistemaRecomendacionPeliculas();
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		try 
		{
			return cargar.cargarInfoJson();
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRaitings) {
		return cargar.cargarRatingsSR(rutaRaitings);
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		return cargar.cargarTagsSR(rutaTags);
	}

	@Override
	public int sizeMoviesSR() {
		int count = 0;
		for(int i = 0; i<cargar.darPeliculasJson().size(); i++)
		{
			if(cargar.darPeliculasJson().darElemento(i).getMovieId()!=null)
			{
				count++;
			}
		}
		return count;
	}

	@Override
	public int sizeUsersSR() {
		int count = 0;
		for(int i = 0; i<cargar.darUsuarios().size(); i++)
		{
			if(cargar.darUsuarios().darElemento(i).getIdUsuario()!=0)
			{
				count++;
			}
		}
		return count;
	}

	@Override
	public int sizeTagsSR() {
		int count = 0;
		for(int i = 0; i<cargar.darTags().size(); i++)
		{
			if(cargar.darTags().darElemento(i).getTag()!=null)
			{
				count++;
			}
		}
		return count;
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) {
		if(idUsuario!=null&&ruta==null)
		{
			ClaseRegistrarSolicitud solicitud = new ClaseRegistrarSolicitud(idUsuario);
			solicitud.setIdUsuario(idUsuario);
			solicitudes.insert(solicitud);
		}
		else if(idUsuario==null&&ruta!=null)
		{
			ClaseRegistrarSolicitud solicitud = new ClaseRegistrarSolicitud(ruta);
			solicitud.setRuta(ruta);
			solicitudes.insert(solicitud);
		}
	}

	@Override
	public void generarRespuestasRecomendaciones() {

		

	}

	
	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> retornar = (ILista<VOPelicula>) new ListaEncadenada();
		retornar = (ILista<VOPelicula>) cargar.peliculasGenero(genero, fechaInicial, fechaFinal);
		return retornar;
	}

	@Override
	public ListaEncadenada<VOUsuarioPelicula> agregarRatingConError(int idUsuario, int idPelicula, Double rating) {
		return cargar.agregarRatingConError(idUsuario, idPelicula, rating);

	}

	@Override
	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clasificarUsuariosPorSegmento() {
		Cargador r6 = new Cargador();
		r6.SegmentarUsuarios();
	}

	private void toArray(){
		ListaEncadenada<VOInfoPelicula> lista = cargar.darPeliculasJson();
		peliculas = new VOInfoPelicula[lista.size()]; 
		for (int i = 0; i < lista.size(); i++) {
			peliculas[i] = lista.darElemento(i); 
		}
	}
	@Override
	public void ordenarPeliculasPorAnho() throws ParseException {
		toArray();
		SimpleDateFormat k= new SimpleDateFormat("dd/M/YYYY");
		arbolOrdenAnho = new ARN<Integer, SeparateChainingHashST<String, ARN<Double,ListaSimpleEncadenada<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>>>>();
		for(VOInfoPelicula c : peliculas){
			int anho=0;
			Date d=k.parse(c.getImdbData().getReleased());
			int id=Integer.parseInt(c.getMovieId());
			if(c.getImdbData().getYear().length()>4)anho=Integer.parseInt(c.getImdbData().getYear()); 
			else if(c.getImdbData().getYear().equals("N/A"))continue;
			else{ 				
				String temp[]=c.getImdbData().getYear().split("-");
				anho=Integer.parseInt(temp[0]);
			}
			String gen=c.getImdbData().getGenre();
			VOPelicula x=new VOPelicula();
			x.setNombre(c.getImdbData().getTitle());
			x.setRatingIMBD(Double.parseDouble(c.getImdbData().getImdbRating()));
			x.setVotostotales(Integer.parseInt(c.getImdbData().getImdbVotes()));
			x.setFechaLanzamineto(d);
//			x.setPais(c.getImdbData().getCountry());
			if(arbolOrdenAnho.contains(anho))
			{
				if(arbolOrdenAnho.get(anho).contains(gen))
				{
					if(arbolOrdenAnho.get(anho).get(gen).contains(x.getRatingIMBD()))
					{
						if(arbolOrdenAnho.get(anho).get(gen).get(x.getRatingIMBD())!=null) arbolOrdenAnho.get(anho).get(gen).get(x.getRatingIMBD()).add(x, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
						else;
					}
					else
					{
						ListaSimpleEncadenada<VOPelicula, SeparateChainingHashST<Integer, VOUsuarioPelicula>>list=new ListaSimpleEncadenada<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>();
						list.add(x, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
						arbolOrdenAnho.get(anho).get(gen).put(x.getRatingIMBD(), list);
					}
				}
				else
				{
					ListaSimpleEncadenada<VOPelicula, SeparateChainingHashST<Integer, VOUsuarioPelicula>>list=new ListaSimpleEncadenada<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>();
					list.add(x, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
					ARN three=new ARN<Double,ListaSimpleEncadenada>();
					three.put(x.getRatingIMBD(), list);
					arbolOrdenAnho.get(anho).put(gen, three);
				}
			}
			else
			{
				ListaSimpleEncadenada<VOPelicula, SeparateChainingHashST<Integer, VOUsuarioPelicula>>list=new ListaSimpleEncadenada<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>();
				list.add(x, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
				ARN<Double, ListaSimpleEncadenada> three=new ARN<Double,ListaSimpleEncadenada>();
				three.put(x.getRatingIMBD(), list);
				SeparateChainingHashST table=new SeparateChainingHashST<>(sizeUsersSR());
				table.put(gen, three);
				arbolOrdenAnho.put(anho, table);

			}
		}

	}



	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial,
			Date fechaFinal) {
		return peliculasGenero(genero, fechaInicial, fechaFinal);
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {
		// TODO Auto-generated method stub
		return null;
	}

}
