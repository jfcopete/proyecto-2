package modelo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.Gson;

import API.ILista;
import data_structures.ARN;
import data_structures.EncadenamientoSeparadoGenero;
import data_structures.ListaEncadenada;
import data_structures.SeparateChainingHashST;
import data_structures.SequentialSearchST;
import vos.QuickSortTags;
import vos.VOGeneroPelicula;
import vos.VOInfoPelicula;
import vos.VOPelicula;
import vos.VORating;
import vos.VOTag;
import vos.VOTagSegmento;
import vos.VOUsuario;
import vos.VOUsuarioPelicula;

public class Cargador {
	private final static String CONFORME="conforme";
	private final static String INCONFORME="inconforme";
	private final static String NEUTRAL="neutral";
	private final static String NOCLASIFICADO="no-clasificado";
	private ListaEncadenada<VOTag> tags = new ListaEncadenada<>();
	private ListaEncadenada<VOTag> tagsAux = new ListaEncadenada<>();
	private ListaEncadenada<VOTagSegmento> tagsSegmento = new ListaEncadenada<>();
	private ListaEncadenada<VORating> ratingss = new ListaEncadenada<>();
	private ListaEncadenada<VOUsuario> users = new ListaEncadenada<>();
	private VOUsuario usuarios[] = new VOUsuario[800];
	private ListaEncadenada<VOInfoPelicula> peliculasJson = new ListaEncadenada<>(); 
	private VOPelicula peliculas[]= new VOPelicula[10000];
	private EncadenamientoSeparadoGenero<String, ARN<Date, VOPelicula>> peliculasGenero = new EncadenamientoSeparadoGenero<>(20);
	private Object[] orden = new Object[2];



	public boolean cargarCategoriasTags(String Ruta)
	{
		try(BufferedReader br = new BufferedReader(new FileReader(Ruta));)
		{

			String line = br.readLine();
			while (line!=null)
			{
				if (line.startsWith("tags")) {
					line = br.readLine();
				}
				String partes[] = line.split(",");
				VOTagSegmento tagSegmento = new VOTagSegmento();
				tagSegmento.setTag(partes[0]);
				tagSegmento.setSegmento(partes[1]);
				tagsSegmento.agregarElementoPrincipio(tagSegmento);
				line=br.readLine();

			}
			if (tagsSegmento.darNumeroElementos()!=0) {
				return true;
			}
		} 

		catch (Exception e) 
		{
			// TODO: handle exception
		}
		return true;
	}
	public boolean cargarRatingsSR(String rutaRatings) {
		users = new ListaEncadenada<VOUsuario>();
		try(BufferedReader br = new BufferedReader(new FileReader(rutaRatings));) {
			String line = br.readLine();
			while (line!=null) {
				VORating VoRating = new VORating();
				if (line.startsWith("u"))line = br.readLine();
				String ratings[]=line.split(",");
				long UsuarioID = Long.parseLong(ratings[0]);
				long PeliculaID = Long.parseLong(ratings[1]);
				double rating = Double.parseDouble(ratings[2]);
				long timestamp = Long.parseLong(ratings[3]);
				VoRating.setIdUsuario(UsuarioID);
				VoRating.setIdPelicula(PeliculaID);
				VoRating.setRating(rating);
				VoRating.setTimestamp(timestamp);
				ratingss.agregarElementoPrincipio(VoRating);
				line=br.readLine();

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	public boolean cargarTagsSR(String rutaTags) {
		try (BufferedReader br = new BufferedReader(new FileReader(rutaTags));) {
			String line = br.readLine();
			while (line!=null) {
				VOTag VoTag = new VOTag();
				if (line.startsWith("u"))line = br.readLine();
				if (line.contains("\"")) {
					int cantidad = cantidadComas(line);
					if (cantidad==5) {
						String pegar[] = line.split(",");
						String tag = pegar[2]+pegar[3]+pegar[4];
						long timestamp = Long.parseLong(pegar[5]);
						VoTag.setTag(tag);
						VoTag.setTimestamp(timestamp);
						tags.agregarElementoPrincipio(VoTag);
						line = br.readLine();
					}
					if (cantidad==6) {
						String pegar[] = line.split(",");
						String tag = pegar[2]+pegar[3]+pegar[4]+pegar[5];
						long timestamp = Long.parseLong(pegar[6]);
						VoTag.setTag(tag);
						VoTag.setTimestamp(timestamp);
						tags.agregarElementoPrincipio(VoTag);
						line = br.readLine();
					}
				}
				String partsOfTags [] = line.split(",");
				long usuarioID = Long.parseLong(partsOfTags[0]);
				int PeliculaID = Integer.parseInt(partsOfTags[1]);
				VoTag.setIdPelicula(PeliculaID);
				String tag = partsOfTags[2];
				long timestamp = Long.parseLong(partsOfTags[3]);
				VoTag.setTag(tag);
				VoTag.setTimestamp(timestamp);
				VoTag.setIdUsuario(usuarioID);
				tags.agregarElementoPrincipio(VoTag);
				//----------------------------------------////Lista de ratings por peliculas//----------------------------------------//
				line = br.readLine();
			}
			String action = "Ha cargado los tags";
			if (tags.size()!=0) {
				return true;
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}
	/**
	 * Devuelve la cantidad de comas de una linea de string
	 * @param linea
	 * @return cantidad
	 */
	public int cantidadComas(String linea)
	{
		int cantidad=0;
		char lineas[]=linea.toCharArray();
		for (int i = 0; i < lineas.length; i++) {
			if (lineas[i]==',') {
				cantidad++;
			}
		}
		return cantidad;

	}

	public void SegmentarUsuarios()
	{
		Tags();
		int conformeCounter=0;
		int inconformeCounter=0;
		int neutralCounter=0;
		int noClasificadoCounter=0;
		tags.cambiarActualAPrimero();
		tagsSegmento.cambiarActualAPrimero();
		//		for (int i = 0; i < tagsSegmento.darNumeroElementos(); i++) {
		//			VOTag a = new VOTag();
		//			a = agregarSegmento(tagsSegmento.darElementoPosicionActual().getTag());
		//			if (tagsSegmento.darElementoPosicionActual().getSegmento()!=null) {
		//				a.setConformismo(tagsSegmento.darElementoPosicionActual().getSegmento());
		//			}
		//			tagsSegmento.avanzarSiguientePosicion();
		//		}

		for (VOUsuario voUsuario : users) {
			ListaEncadenada<VOTag> usar = new ListaEncadenada<>();
			usar=darTagsUsuario(voUsuario.getIdUsuario());
			if (usar.isEmpty()==true)continue;
			if (usar.darNumeroElementos()!=0) {
				usar.cambiarActualAPrimero();
				for (VOTag voTag : usar) {
					if (voTag.getConformismo().startsWith("con"))
					{
						conformeCounter++;
					}else if (voTag.getConformismo().startsWith("in"))
					{
						inconformeCounter++;
					}else if (voTag.getConformismo().startsWith("neu"))
					{
						neutralCounter++;
					}else if (voTag.getConformismo().startsWith("no-"))
					{
						noClasificadoCounter++;
					}
				}
				int arreglo [] = {conformeCounter,inconformeCounter,neutralCounter,noClasificadoCounter};
				QuickSortTags a = new QuickSortTags();
				a.mergeSort(arreglo);
				if (arreglo[3]==conformeCounter) {
					voUsuario.setConformismo(CONFORME);
				}else if (arreglo[3]==inconformeCounter) {
					voUsuario.setConformismo(INCONFORME);
				}else if (arreglo[3]==neutralCounter) {
					voUsuario.setConformismo(NEUTRAL);
				}else if (arreglo[3]==noClasificadoCounter) {
					voUsuario.setConformismo(NOCLASIFICADO);
				}
			}
		}

	}
	public ListaEncadenada<VOTag> darTagsUsuario(long id) {
		ListaEncadenada<VOTag> retornar = new ListaEncadenada<>();

		tags.cambiarActualAPrimero();
		for (int i = 0; i < tags.darNumeroElementos(); i++) {
			VOTag a = tags.darElementoPosicionActual();
			if (a.getIdUsuario()==id) {
				retornar.agregarElementoPrincipio(a);
			}
			tags.avanzarSiguientePosicion();
		}
		return retornar;
	}
	public VOTag agregarSegmento(String pTag)
	{
		for (VOTag voTag : tags) {
			if (voTag.getTag().equalsIgnoreCase(pTag)) {
				return voTag;
			}
		}
		return null;
	}
	public VOUsuario buscarUsuarioID(long id)
	{
		for (VOUsuario voUsuario : users) {
			if (voUsuario.getIdUsuario()==id) {
				return voUsuario;
			}
		}
		return null;
	}

	public void Tags() {
		for (VOTag voTag : tags) {
			voTag.setConformismo(darSegmento(voTag.getTag()));
		}
	}
	public String darSegmento(String pTag)
	{
		for (VOTagSegmento voTagSegmento : tagsSegmento) {
			if (voTagSegmento.getTag().contains(pTag)) {
				return voTagSegmento.getSegmento();
			}
		}
		return "no-clasificado";
	}







	public void cargarUsuarios()
	{
		ratingss.cambiarActualAPrimero();
		for (int i = 0; i < (ratingss.darNumeroElementos()/5); i++) {
			VOUsuario a = new VOUsuario();
			a.setIdUsuario(ratingss.darElementoPosicionActual().getIdUsuario());
			a.setPrimerTimestamp(ratingss.darElementoPosicionActual().getTimestamp());
			a.setNumRatings(5);
			a.setConformismo("");
			users.agregarElementoPrincipio(a);
			ratingss.avanzarSiguientePosicion();
			ratingss.avanzarSiguientePosicion();
			ratingss.avanzarSiguientePosicion();
			ratingss.avanzarSiguientePosicion();
			ratingss.avanzarSiguientePosicion();

		}
	}
	public ListaEncadenada<VOUsuario> darUsuarios()
	{
		return users;
	}

	public ListaEncadenada<VOTag> darTags() {
		return tags;
	}

	public ListaEncadenada<VOUsuario> darUsuariosPorSegmento(String pSegmento)
	{
		ListaEncadenada<VOUsuario> retornar = new ListaEncadenada<>();
		users.cambiarActualAPrimero();
		for (int i = 0; i < users.darNumeroElementos(); i++) {
			if (users.darElementoPosicionActual().getConformismo().equalsIgnoreCase(pSegmento)) {
				retornar.agregarElementoPrincipio(users.darElementoPosicionActual());
			}
			users.avanzarSiguientePosicion();
		}
		return retornar;
	}

	public ListaEncadenada<VOTagSegmento> darTagsSegmento() {
		return tagsSegmento;
	}

	public boolean cargarInfoJson() throws ParseException{
		Gson g = new Gson(); 
		boolean retorno = false;
		BufferedReader br = null; 
		try {
			br = new BufferedReader(new FileReader("data/links_json.json")); 
			VOInfoPelicula[] resultado = g.fromJson(br, VOInfoPelicula[].class);
			int i=0;
			for (VOInfoPelicula voInfoPelicula : resultado) {
				peliculasJson.agregarElementoFinal(voInfoPelicula);
				voInfoPelicula.getImdbData().getTitle();
				int votos = 0;
				VOPelicula agregar = new VOPelicula();
				String nombre = voInfoPelicula.getImdbData().getTitle();
				DateFormat formatoDelTexto = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
				String format= voInfoPelicula.getImdbData().getReleased();
				Date date = null;
				if (format.equals("N/A")) {
					date= new Date();
				}else{
					date = formatoDelTexto.parse(format);
				}
				String voto[]= (voInfoPelicula.getImdbData().getImdbVotes()).split(",");
				String gvotos ="";
				for (int j = 0; j < voto.length; j++) {
					gvotos+=voto[j];
				}
				if (gvotos.equals("N/A")) {
					votos=0;
				}else{
					votos=Integer.parseInt(gvotos);
				}
				double rating = 0;
				if (voInfoPelicula.getImdbData().getImdbRating().equals("N/A")) {
					rating=0;
				}else{
					rating=	Double.parseDouble(voInfoPelicula.getImdbData().getImdbRating());
				}
				String generos[] = voInfoPelicula.getImdbData().getGenre().split(",");
				ListaEncadenada<VOGeneroPelicula> generosPelicula = new ListaEncadenada<>();
				agregar.setNombre(nombre);
				agregar.setFechaLanzamineto(date);
				//agregar.setPromedioAnualVotos();
				agregar.setRatingIMBD(rating);
				agregar.setGenerosAsociados(generosPelicula);
				agregar.setVotostotales(votos);
				peliculas[i]=agregar;
				ARN<Date,VOPelicula> general = new ARN<>();
				for (int k = 0; k < generos.length; k++) {
					VOGeneroPelicula genero = new VOGeneroPelicula();
					genero.setNombre(generos[k]);
					generosPelicula.agregarElementoPrincipio(genero);
					if (peliculasGenero.get(genero.getNombre())==null) {
						general.put(date, agregar);
						peliculasGenero.put(genero.getNombre().trim(), general);
					}else {
						general = peliculasGenero.get(genero.getNombre());
						general.put(date, agregar);
						peliculasGenero.put(genero.getNombre().trim(), general);
					}
				}
				i++;

				System.out.println(peliculasJson.darElementoPosicionActual().getMovieId() + " " + peliculasJson.darElementoPosicionActual().getImdbData().getTitle());
			}
			retorno = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return retorno;
	}
	public EncadenamientoSeparadoGenero<String, ARN<Date, VOPelicula>> darEncadenamiento()
	{
		return peliculasGenero;
	}
	public ListaEncadenada<VOInfoPelicula> darPeliculasJson(){
		return peliculasJson; 
	}
	public VOPelicula[] darPeliculas()
	{
		return peliculas;
	}
	public ListaEncadenada<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		ListaEncadenada<VOPelicula> retornar = new ListaEncadenada<>();
		ListaEncadenada<VOPelicula> ab = new ListaEncadenada<>();
		if (peliculasGenero.contains(genero.getNombre())) {
			ARN<Date,VOPelicula> arbolDeGenero = peliculasGenero.get(genero.getNombre());
			ListaEncadenada<VOPelicula> az = arbolDeGenero.inOrderList(retornar);
			for (VOPelicula voPelicula : az) {
				Date dates = voPelicula.getFechaLanzamineto();
				if (dates.getTime()>=fechaInicial.getTime()&&dates.getTime()<=fechaFinal.getTime()) {
					ab.agregarElementoPrincipio(voPelicula);
				}
			}
		}
		return ab;
	}
	public ListaEncadenada<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {
		return null;
	public ListaEncadenada<VOUsuarioPelicula> agregarRatingConError(int idUsuario, int idPelicula,
			Double rating) {
		ListaEncadenada<VOUsuarioPelicula> lista=new ListaEncadenada<>();
		Iterator iter=users.iterator();
		while(iter.hasNext())
		{
			VOUsuarioPelicula a=(VOUsuarioPelicula) iter.next();
			if(idUsuario==a.getIdUsuario())
			{
				lista.agregarElementoFinal(a);

			}
		}
		return lista;
		
	}

}

