package data_structures;

public class NodoLista<T>
{
	private T item;

	private NodoLista<T> next;

	public NodoLista(T item)
	{
		next = null;
		this.item = item;
	}


	public NodoLista() {
		// TODO Auto-generated constructor stub
	}


	public T getItem() 
	{
		return item;
	}

	public void setItem(T item) 
	{
		this.item = item;
	}

	public NodoLista<T> getNext() 
	{
		return next;
	}


	public void setNext(NodoLista<T> next) 
	{
		this.next = next;
	}
}
