package data_structures;

import java.util.Iterator;

public class ListaSimpleEncadenada <K, V>
{
	private Node head;
	private class Node
	{
		private Node next;
		private K key;
		private V value;
		private Node( K k,V v)
		{
			this.key=k;
			this.value=v;
		}
	}
	public void add(K k,V v)
	{
		head=add(head,k,v);
	}
	private Node add(Node x,K k, V v)
	{
		if(x==null) return new Node(k,v);
		else x.next=add(x.next,k,v);
		return x;
	}
	public int size()
	{
		return size(head);
	}
	private int size(Node x)
	{
		if(x==null) return 0;
		else return 1+size(x.next);
	}
	public V get(int pos)
	{
		return get(head,pos,1).value;
	}
	private Node get (Node x,int pos, int act)
	{
		if(x==null) return null;
		else if(act==pos) return x;
		else return get(x.next,pos,act+1);
	}
	public V getEnd()
	{
		return getEnd(head).value;
	}
	private Node getEnd(Node x)
	{
		if(x==null) return null;
		else if(x.next!=null) return getEnd(x.next);
		else return x;
	}
	public void remove(int pos)
	{
		head=remove(head,pos,1);
	}
	public Node remove(Node x,int pos,int act)
	{
		if(x==null)return null;
		else if(pos==act) return x.next;
		else if(pos>act+1) remove(x.next,pos,act+1);
		else
		{
			if(x.next!=null)x.next=x.next.next;
			else;
		} 			
		return x;	
	}
	public Iterator iterator()
	{
		return new Iterator<K>(){
			Node actual=head;
			int m=0;
			@Override
			public boolean hasNext() {
				return actual.next==null;
			}

			@Override
			public K next() {
				if(m==0){
				return actual.key;
				}
				else
				{
					actual=actual.next;
					return actual.key;
				}
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
		};
	}
}