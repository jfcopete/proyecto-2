package data_structures;

public class ColaPrioridad<T extends Comparable<T>> {
	private T[] pq;
	private int n;

	public ColaPrioridad(int capacidad)
	{
		pq = (T[]) new Comparable[capacidad +1];
	}

	public ColaPrioridad()
	{
		this(1);
	}

	public boolean isEmpty() {
		return n == 0;
	}

	public int size() {
		return n;
	}

	public T max() {

		return pq[1];
	}

//	private void resize(int capacity) {
//		assert capacity > n;
//		T[] temp = (T[]) new Object[capacity];
//		for (int i = 1; i <= n; i++) {
//			temp[i] = pq[i];
//		}
//		pq = temp;
//	}
	
	 public void insert(T x) {
	        pq[++n] = x;
	        swim(n);
	    }
	 
	 public T delMax() {
	        
	        T max = pq[1];
	        exch(1, n--);
	        sink(1);
	        pq[n+1] = null;    
	        return max;
	    }
	 
	 private void swim(int k) {
	        while (k > 1 && less(k/2, k)) {
	            exch(k, k/2);
	            k = k/2;
	        }
	    }

	    private void sink(int k) {
	        while (2*k <= n) {
	            int j = 2*k;
	            if (j < n && less(j, j+1)) j++;
	            if (!less(k, j)) break;
	            exch(k, j);
	            k = j;
	        }
	    }
	    
	    private boolean less(int i, int j) {
	        return pq[i].compareTo(pq[j]) < 0;
	    }

	    private void exch(int i, int j) {
	        T swap = pq[i];
	        pq[i] = pq[j];
	        pq[j] = swap;
	    }
	    
	    private boolean isMaxHeap() {
	        return isMaxHeap(1);
	    }
	    
	    private boolean isMaxHeap(int k) {
	        if (k > n) return true;
	        int left = 2*k;
	        int right = 2*k + 1;
	        if (left  <= n && less(k, left))  return false;
	        if (right <= n && less(k, right)) return false;
	        return isMaxHeap(left) && isMaxHeap(right);
	    }

}
