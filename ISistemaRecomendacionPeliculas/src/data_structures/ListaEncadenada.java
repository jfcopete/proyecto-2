package data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T>{

	private NodoLista<T> primero;
	private NodoLista<T> actual;
	private NodoLista<T> next; 
	private int listSize;
	
	public ListaEncadenada()
	{
		primero = new NodoLista<T>(null);
		actual = primero;
		listSize=0;
	}
	
	public void cambiarActualAPrimero()
	{
		actual=primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
			{
			private NodoLista<T> act=null;
	
			@Override
			public boolean hasNext() 
			{
				if(act==null)return primero.getItem()!=null;
				else return act.getNext() != null;
			}
	
			@Override
			public T next() 
			{
				if(act==null)
				{
					act=primero;
					if(act==null)return null;
					else return act.getItem();
				}
				else
				{
					act = act.getNext();
					return act.getItem();
				}
	
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
}
	public NodoLista<T> darPrimero(){return primero;}
	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			NodoLista<T> act = primero;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setNext(new NodoLista<T>(elem));
					act.getNext().setItem(elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}
	
	public void agregarElementoPrincipio(T elem)
	{
		NodoLista<T> newNode = new NodoLista(elem);
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			newNode.setNext(primero);
			primero=newNode;
		}
		listSize++;
	}
	
	public T quitarElementoPrincipio()
	{
		if(primero == null)
		{
			System.out.println("No hay elementos");
		}
		T elem = primero.getItem();
		NodoLista<T> siguientePrimero = primero.getNext();
		primero.setItem(null);
		primero = siguientePrimero;
		listSize--;
		return elem;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		NodoLista<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	
	public boolean retrocederPosicionAnterior() 
	{
		NodoLista<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}
	
	public boolean isEmpty() {
		if (primero==null) {
			return true;
		}
		return false;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	public void modificar(T darElemento, int i) {
		// TODO Auto-generated method stub
		NodoLista<T> tempActual = primero;
		int contador = 0;
		while(contador<i)
		{
			tempActual = tempActual.getNext();
			contador++;
		}
		tempActual.setItem(darElemento);
	}

}
