package data_structures;

public class BST <Key extends Comparable<Key>, Value >{
	private NodoArbol root; 
	public class NodoArbol{
		private Key key; 
		private Value val; 
		private NodoArbol izquierdo, derecho; 
		private int N; 
		public NodoArbol(Key key, Value val, int N){this.key = key; this.val=val; this.N = N;}

	}
	public int size(){
		return size(root); 
	}
	public int size(NodoArbol x){
		if (x == null)return 0; 
		else return x.N; 
	}
	public Value get(Key key){
		return get(root, key); 
	}
	public Value get(NodoArbol x , Key key){
		if (x==null) return null; 
		int cmp = key.compareTo(x.key); 
		if (cmp < 0) return get(x.izquierdo, key); 
		if (cmp>0) return get(x.derecho, key); 
		else return x.val; 
	}
	private NodoArbol put(NodoArbol x, Key key, Value val){ 
		if (x == null) return new NodoArbol(key, val, 1); 
		int cmp = key.compareTo(x.key); 
		if (cmp < 0) x.izquierdo = put(x.izquierdo, key, val); 
		if (cmp > 0) x.derecho = put(x.derecho, key, val); 
		else x.val = val; 
		x.N = size(x.izquierdo) + size(x.derecho) + 1; 
		return x; 

	}
	public Key min(){
		return min(root).key;
	}
	public NodoArbol min(NodoArbol x){ 
		if (x.izquierdo == null) return x; 
		return min(x.izquierdo); 
	}
	public Key max(){
		return max(root).key; 
	}
	public NodoArbol max (NodoArbol x){
		if (x.derecho == null) return x; 
		return max(x.derecho); 
	}
	public Key floor(Key key){
		NodoArbol x = floor(root, key); 
		if (x == null) return null; 
		return x.key; 
	}
	public NodoArbol floor(NodoArbol x, Key key){ 
		if (x==null) return null; 
		int cmp = key.compareTo(x.key); 
		if (cmp == 0) return x; 
		if (cmp < 0) return floor(x.izquierdo, key); 
		NodoArbol t = floor(x.derecho, key); 
		if (t != null) return t; 
		else return x; 
	}
	public Key select(int k){
		return select(root, k).key; 
	}
	private NodoArbol select(NodoArbol x, int k){
		if (x==null) return null; 
		int t = size(x.izquierdo); 
		if (t > k) return select(x.izquierdo, k); 
		else if (t> k) return select(x.derecho, k-t-1);
		else return x; 
	}
	public int rank(Key key){
		return rank(key, root); 
	}
	public int rank(Key key, NodoArbol x){
		if (x==null) return 0; 
		int cmp = key.compareTo(x.key); 
		if (cmp < 0) return rank(key, x.izquierdo); 
		else if (cmp > 0) return 1 + size(x.izquierdo) + rank(key, x.derecho); 
		else return size(x.izquierdo); 
	}
	public void deleteMin(){
		root = deleteMin(root); 
	}
	private NodoArbol deleteMin(NodoArbol x){
		if (x.izquierdo == null) return x.derecho; 
		x.izquierdo = deleteMin(x.izquierdo); 
		x.N = size(x.izquierdo) + size(x.derecho) + 1 ; 
		return x; 
	}
	public void delete(Key key){

	}
	public NodoArbol delete(NodoArbol x, Key key){ 
		if (x==null)return null; 
		int cmp = key.compareTo(x.key); 
		if (cmp<0) x.izquierdo = delete(x.izquierdo, key);
		else if (cmp>0) x.derecho = delete(x.derecho, key);
		else{
			if (x.derecho == null) return x.izquierdo; 
			if (x.izquierdo== null) return x.derecho; 
			NodoArbol t = x; 
			x = min(t.derecho); 
			x.derecho = deleteMin(t.derecho); 
			x.izquierdo = t.izquierdo; 
		}
		x.N = size(x.izquierdo) + size(x.derecho) + 1; 
		return x; 
	}
	public Iterable<Key> keys(){
		return keys(min(), max());
	}
	public Iterable<Key> keys(Key lo, Key hi){
		Queue<Key> queue = new Queue<>(); 
		keys(root, queue, lo, hi); 
		return (Iterable<Key>) queue; 
	}
	private void keys(NodoArbol x, Queue<Key> queue, Key lo, Key hi)
	{
		if (x == null) return;
		int cmplo = lo.compareTo(x.key);
		int cmphi = hi.compareTo(x.key);
		if (cmplo < 0) keys(x.izquierdo, queue, lo, hi);
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key);
		if (cmphi > 0) keys(x.izquierdo, queue, lo, hi);
	}
}
