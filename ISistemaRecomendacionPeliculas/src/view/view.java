package view;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedDeque;

import controller.Controller;
import data_structures.ARN;
import data_structures.EncadenamientoSeparadoGenero;
import data_structures.ListaEncadenada;
import data_structures.Queue;
import vos.VOGeneroPelicula;
import vos.VOInfoPelicula;
import vos.VOPelicula;
import vos.VOTag;
import vos.VOTagSegmento;
import vos.VOUsuario;

public class view {
	private final static String Ruta_TAGS="./data/tags.csv";
	private final static String RUTA_RATINGS="./data/newRatings.csv";
	private final static String Ruta_TAGSCATEGORIASS="./data/categoriestagsv2.csv";

	private static void printMenu(){
		System.out.println("1. Cargar Tags,Ratings y usuarios");
		System.out.println("2. para imprimir Tags");
		System.out.println("3. para imprimir Usuarios");
		System.out.println("4. Segmenta e imprime los usuarios");
		System.out.println("5. Imprime las peliculas");
		System.out.println("6. Imprime el tama�o del Encadenamiento Req3");


	}

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		boolean fin = false;
		while(fin==false){
			printMenu();
			Scanner sc = new Scanner(System.in);
			int option = sc.nextInt();

			switch (option) {

			case 1:
				Controller.cargarCategorias(Ruta_TAGSCATEGORIASS);
				Controller.cargarTags(Ruta_TAGS);
				Controller.cargarRatings(RUTA_RATINGS);
				Controller.cargarInfoJson();
				Controller.cargarUsuarios();
				break;
			case 2:
				ListaEncadenada<VOTag> j = Controller.darTags();
				for (VOTag voTag : j) {
					System.out.println(""+voTag.getTag());
				}
				break;
			case 3:
				ListaEncadenada<VOUsuario> usuarios = Controller.darUsuarios();
				for (VOUsuario voUsuario : usuarios) {
					System.out.println(""+voUsuario.getIdUsuario()+" "+voUsuario.getPrimerTimestamp()+" "+voUsuario.getNumRatings());
				}
				System.err.println(""+usuarios.darNumeroElementos());
				break;
			case 4:
				Controller.segmentarUsuarios();
				System.out.println("Introduzca el segmento");
				Scanner segmento = new Scanner(System.in);
				String pSegmento = segmento.nextLine();
				ListaEncadenada<VOUsuario> segmentados = new ListaEncadenada<>();
				segmentados = Controller.darUsuariosPorSegmento(pSegmento);
				for (VOUsuario voUsuario : segmentados) {
					System.out.println(" "+voUsuario.getIdUsuario()+" "+voUsuario.getConformismo());					
				}
				System.err.println(""+segmentados.darNumeroElementos());
				break;
			case 5:
				VOPelicula [] pelis = Controller.darPelis();
				int counter=0;
				for (int i = 0; i < pelis.length; i++) {
					if (pelis[i]!=null) {
						counter++;
					}
				}
				System.err.println(""+counter);
				break;
			case 6:
				ListaEncadenada<String> keys = Controller.darEncadenamiento().keysInAList();
				for (String string : keys) {
					System.out.println(string);
				}
				break;
			case 7:
				VOGeneroPelicula genero = new VOGeneroPelicula();
				genero.setNombre("Comedy");
				DateFormat formatoDelTexto = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
				String format= "01 Jan 2000";
				Date date1 = null;
				if (format.equals("N/A")) {
					date1= new Date();
				}else{
					date1 = formatoDelTexto.parse(format);
				}
				String format2= "15 Dec 2011";
				Date date2 = null;
				if (format2.equals("N/A")) {
					date2= new Date();
				}else{
					date2 = formatoDelTexto.parse(format2);
				}
				Date a = new Date();
				ListaEncadenada<VOPelicula> peliculasRango = new ListaEncadenada<>();
				peliculasRango = Controller.darPeliculasRango(genero, date1, date2);
				for (VOPelicula voPelicula : peliculasRango) {
					System.out.println(""+voPelicula.getNombre()+"  "+voPelicula.getFechaLanzamineto().toString());
				}
				System.err.println("Cantidad de Peliculas : "+""+peliculasRango.darNumeroElementos());
				break;
			}

		}

	}
}
